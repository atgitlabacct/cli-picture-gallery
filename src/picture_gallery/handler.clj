(ns picture-gallery.handler
  (:require [compojure.route :as route]
            [compojure.core :refer [defroutes routes GET]]

            [ring.middleware stacktrace reload]

            [noir.util.middleware :as noir-middleware]
            [noir.session :as session]

            [taoensso.timbre :as timbre]
            [com.postspectacular.rotor :as rotor]
            [environ.core :refer [env]]

            [picture-gallery.routes.gallery :refer [gallery-routes]]
            [picture-gallery.routes.home :refer [home-routes]]
            [picture-gallery.routes.upload :refer [upload-routes]]
            [picture-gallery.routes.auth :refer [auth-routes]]))

(defn info-appender
  [{:keys [level message]}]
  (println "level:" level "message:" message))

(defn init []
  #_(timbre/set-config! 
    [:appenders :rotor]
    {:min-level :info
     :enabled? true
     :async? false
     :max-message-per-msecs nil
     :fn rotor/append})

  (timbre/set-config! 
    [:shared-appender-config :rotor]
     {:path "log/error.log"
      :max-size (* 512 1024) 
      :backlog 10})
  (println "db-url: " (env :db-url))
  (println "db-user: " (env :db-user))
  (println "db-pass: " (env :db-pass))
  (println "galleries path" (env :galleries-path))
  (timbre/error "Start error file")
  (timbre/info "picture-gallery started successfully"))

(defn destroy []
  (timbre/info "picture-gallery is shutting down"))

(defn user-page
  [_]
  (session/get :user))

(defroutes app-routes
  (GET "/test" [] "OK")
  (route/resources "/")
  (route/not-found "Not Found"))

(def app
  (-> (noir-middleware/app-handler [home-routes 
                                    gallery-routes
                                    auth-routes 
                                    upload-routes
                                    app-routes] :access-rules [user-page])
      (ring.middleware.reload/wrap-reload)
      (ring.middleware.stacktrace/wrap-stacktrace)))
