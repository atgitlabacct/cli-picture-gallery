(ns picture-gallery.models.db
  (:require [clojure.java.jdbc :as sql]
            [clojure.string :refer [blank?]]
            [korma.db :refer [defdb transaction mysql]]
            [korma.core :refer :all]
            [environ.core :refer [env]]))

(def db-spec
  {:subprotocol "mysql"
   :subname (env :db-url)
   :user (env :db-user)
   :password (env :db-pass)})

(defdb korma-db (mysql {:db "gallery"
                        :user "root"
                        :password "root"
                        :host "localhost"
                        :port 3306}))
(declare users images)

(defentity users
  (has-many images {:fk :userid}))

(defentity images
  (belongs-to users {:fk :userid}))

(defn delete-image
  [userid name]
  (delete images (where {:userid userid :name name})))

(defn get-gallery-previews
  []
  (sql/with-db-transaction [tran-conn db-spec]
    (sql/execute! tran-conn ["SET @row_number:=0;"])
    (sql/query tran-conn
               ["SELECT * FROM 
                (SELECT *, @row_number:=@row_number+1 AS row_number 
                FROM images ORDER BY name) as rows
                where row_number = 1"])))

(defn image-exists?
  [userid name]
  (not (empty? (select images (where {:userid userid :name name})))))

(defn images-by-user
  [userid]
  (select images (where {:userid userid})))

(defn add-image
  [userid name]
  (transaction
    (if (empty? (select images
                        (where {:userid userid :name name})
                        (limit 1)))
      (insert images (values {:userid userid :name name}))
      (throw
        (Exception. "you have already uploaded an image with the same name")))))

(defn get-user
  [id]
  (first (select users
                 (where {:id id})
                 (limit 1))))

(defn create-user
  [user]
  (insert users (values user)))

(defn delete-user
  [id]
  (delete users (where {:id id})))

(comment
  (insert images (korma.core/values {:userid "jdoe", :name "foobar"}))
  (insert images (korma.core/values {:userid "jdoe", :name "barbaz"}))
  (image-exists? "jdoe" "China.png")

  (create-user {:id "jdoe" :pass "foobar"})
  (get-user "jdoe")
  (select users (with images))
  (select images (with users))

  (delete users)
  (delete images)

  (get-gallery-previews)

  (images-by-user "jdoe"))
