(ns picture-gallery.util
  (:require [noir.session :as session]
            [noir.request :as request]

            [environ.core :refer [env]]
            [hiccup.util :refer [url-encode]])
  (:import java.io.File))

(def thumb-size 150)

(def thumb-prefix "thumb_")

(def galleries (env :galleries-path))
 
(defn context
  []
  (try
    (println "the request params are: " request/*request*)
    (request/*request* :context)
    (catch Exception ex
      (println "Exception: " (.getMessage ex))))) 

(defn gallery-path
  []
  (str galleries File/separator (session/get :user))) 
 
(defn image-uri
  [userid file-name]
  (str "/img/" userid "/" (url-encode file-name)))

(defn thumb-uri
  [userid file-name]
  (image-uri userid (str thumb-prefix file-name)))

