(ns picture-gallery.routes.home
  (:require [compojure.core :refer :all]

            [picture-gallery.views.layout :as layout]
            [picture-gallery.util :as util]
            [picture-gallery.models.db :as db]

            [taoensso.timbre :refer [trace debug info warn error fatal]]

            [noir.session :as session]))

(defn home []
  (layout/render "home.html"
                 {:thumb-prefix util/thumb-prefix
                  :galleries (db/get-gallery-previews)}))

(defroutes home-routes
  (GET "/" [] (home)))
