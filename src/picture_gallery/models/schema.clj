(ns picture-gallery.models.schema
  (:require [picture-gallery.models.db :refer :all]
            [clojure.java.jdbc :as sql]))


(defn create-images-table
  []
  (try
    (sql/db-do-commands 
      db-spec
      (sql/create-table-ddl
        :images
        [:id "INT NOT NULL PRIMARY KEY AUTO_INCREMENT"]
        [:userid "varchar(32)"]
        [:name "varchar(100)"]))
    (catch java.sql.BatchUpdateException ex
      (println (.getMessage ex)))))

(defn create-users-table
  []
  (sql/db-do-commands db-spec
    (sql/create-table-ddl
      :users
      [:id "varchar(32) PRIMARY KEY"]
      [:pass "varchar(100)"])))


(comment
  (sql/query db-spec ["SELECT * FROM users"])
  (create-users-table)
  (create-images-table)
  (sql/db-do-commands
    db-spec 
    (sql/drop-table-ddl :users)))
