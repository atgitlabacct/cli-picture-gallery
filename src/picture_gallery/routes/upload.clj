(ns picture-gallery.routes.upload
  (:require [compojure.core :refer [defroutes GET POST]]

            [picture-gallery.models.db :as db]
            [picture-gallery.views.layout :as layout]
            [picture-gallery.util :refer [gallery-path galleries 
                                          thumb-prefix thumb-uri thumb-size]]

            [taoensso.timbre :refer [trace debug info warn error fatal]]

            [noir.session :as session]
            [noir.response :as resp]
            [noir.util.route :refer [restricted]]
            [noir.io]

            [clojure.java.io :as io]
            [ring.util.response :refer [file-response]])
  (:import [java.io File FileInputStream FileOutputStream]
           [java.awt.image AffineTransformOp BufferedImage]
           java.awt.RenderingHints
           java.awt.geom.AffineTransform
           javax.imageio.ImageIO
           java.io.File))

(defn delete-image
  [userid name]
  (try
    (db/delete-image userid name)
    (io/delete-file (str (gallery-path) File/separator name))
    (io/delete-file (str (gallery-path) File/separator thumb-prefix name))
    "ok"
    (catch Exception ex 
      (error ex "an error has occured while deleting" name)
      (.getMessage ex))))

(defn delete-images
  [names]
  (let [userid (session/get :user)]
    (resp/json
      (for [name names] {:name name :status (delete-image userid name)}))))

(defn serve-file
  [user-id file-name]
  (file-response (str galleries File/separator 
                      user-id File/separator 
                      file-name)))

(defn scale
  [img ratio width height]
  (let [scale (AffineTransform/getScaleInstance (double ratio) (double ratio)) 
        transform-op (AffineTransformOp. scale AffineTransformOp/TYPE_BILINEAR)]
    (.filter transform-op img (BufferedImage. width height (.getType img)))))

(defn scale-image
  [file]
  (let [img   (ImageIO/read file)
        img-width (.getWidth img)
        img-height (.getHeight img)
        ratio (/ thumb-size img-height)]
    (scale img ratio (int (* img-width ratio)) thumb-size)))

(defn save-thumbnail
  [{:keys [filename]}]
  (let [path (str (gallery-path) File/separator)]
    (ImageIO/write
      (scale-image (io/input-stream (str path filename)))
      "jpeg"
      (File. (str path thumb-prefix filename)))))
 
(defn upload-page
  [params]
  (layout/render "upload.html"
                 params))

(defn handle-upload
  [{:keys [file] {:keys [filename]} :file}]
  (upload-page
    (if (empty? filename)
      "please select a file to upload"
      (try
        (noir.io/upload-file (gallery-path) file)
        (save-thumbnail file)
        (db/add-image (session/get :user) filename)
        {:image (thumb-uri (session/get :user) filename)}
        (catch Exception ex
          (error ex "an error has occured while uploading" name)
          {:error (str "error uploading file " (.getMessage ex))})))))

(defroutes upload-routes
  (GET "/img/:user-id/:file-name" [user-id file-name] (serve-file user-id file-name))
  (GET "/upload" [info] (restricted (upload-page {:info info})))
  (POST "/upload" {params :params} (restricted (handle-upload params)))
  (POST "/delete" [names] (delete-images names)))

(comment
  (let [{:keys [file file/filename]} foo]
    (println "File is " file)
    (println "Filename is " filename))

  (ImageIO/write
    (scale (ImageIO/read (io/input-stream "image.jpg")) 0.5 150 150) "jpeg" (File. "foo.jpg"))

  (ImageIO/write
    (scale-image (io/input-stream "image.jpg")) "jpeg" (File. "scaled.jpg"))
  )
