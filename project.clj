(defproject picture-gallery "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [compojure "1.1.6"]
                 [ring-server "0.3.1"]
                 [lib-noir "0.8.2"]

                 [mysql/mysql-connector-java "5.1.6"]

                 [korma "0.3.0"]
                 [selmer "0.7.7"]
                 [log4j "1.2.15" :exclusions [javax.mail/mail
                                              javax.jms/jms
                                              com.sun.jdmk/jmxtools
                                              com.sun.jmx/jmxri]]

                 [com.postspectacular/rotor "0.1.0"]
                 [com.taoensso/timbre "3.3.1"]
                 [environ "0.4.0"]]
  :plugins [[lein-ring "0.8.12"]
            [lein-environ "0.4.0"]]
  :ring {:handler picture-gallery.handler/app
         :init picture-gallery.handler/init
         :destroy picture-gallery.handler/destroy}
  :repl-options { :port 50001}
  :profiles
  {:uberjar {:aot :all}
   :production {:ring {:open-browser? false, 
                       :stacktraces? false, 
                       :auto-reload? false}
                :env {:port 3000
                      :db-url "//localhost:3306/gallery"
                      :db-user "root"
                      :db-pass "root"
                      :galleries-path "galleries"}}
   :dev {:dependencies [[ring-mock "0.1.5"] 
                        [ring/ring-devel "1.3.1"]]
         :env {:port 3000
               :db-url "//127.0.0.1:3306/gallery"
               :db-user "root"
               :db-pass "root"
               :galleries-path "galleries"}}})
